import React from 'react';
// import logo from './logo.svg';
import './App.css';
import { Switch, Route } from 'react-router-dom'
import AddKendaraan from './Components/AddKendaraan';
import Home from './Components/Home';
import Nav from './Components/Nav';
import GerbangDua from './Components/GerbangDua';
import GerbangTiga from './Components/GerbangTiga';

function App() {
  return (
    <div className="App">
      <Nav />
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/addkendaraan" component={AddKendaraan} />
        <Route path="/gerbangdua" component={GerbangDua} />
        <Route path="/gerbangtiga" component={GerbangTiga} />
      </Switch>
    </div>
  );
}

export default App;
