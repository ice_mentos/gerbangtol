import React, { Component } from 'react'

export default class GerbangTiga extends Component {
    state = {
        hargam: 10000,
        hargab: 30000,
        hargat: 40000
    }


    render() {
        return (
            <div>
                <br />
                <br />
                <div className="card">
                    <div className="card-header">
                        <center><h2>Gerbang Tiga</h2></center>
                    </div>
                    <div className="container">

                        <div className="card-body">
                            <form>
                                <div className="form-group">
                                    <label>Plat Nomor</label><br />
                                    <input type="text" name="platNo" placeholder="Plat Nomor Polisi" className="form-control" onChange={this.handleChange} />
                                </div>
                                <div className="form-group">
                                    <label>Type Kendaraan</label>
                                    {/* <input type="text" name="perusahaan" placeholder="" className="form-control" onChange={this.handleChange} /><br /><br /> */}
                                    <select className="form-control" name="typeKendaraan">
                                        <option>Pilih..</option>
                                        <option>Mobil</option>
                                        <option>Bus</option>
                                        <option>Truk</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label>Harga</label><br />
                                    <input type="text" name="platNo" placeholder="Total Bayar" className="form-control" />
                                </div>
                                <button className="btn btn-primary" type="submit">Submit</button><br /><br />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
