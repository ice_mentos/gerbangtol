import React, { Component } from 'react'
import Axios from 'axios';
import { Link } from 'react-router-dom';

export default class Home extends Component {
    constructor() {
        super();
        this.state = {
            kendaraan: []
        }
    }

    componentDidMount() {
        Axios.get('http://localhost:8181/kendaraan')
            .then(res => {
                console.log(res);
                const kendaraan = res.data;
                this.setState({
                    kendaraan
                })
            })
    }

    render() {
        // console.log(this.state.kendaraan);
        return (
            <>
                <div className="container">
                    <br />
                    <div className="row">
                        <h2 className="">Pintu Tol</h2><br /><br />
                    </div>
                    <div className="float-right">
                        {/* <button className="btn btn-success">Gerbang I</button>
                        <button className="btn btn-primary">Gerbang II</button> */}
                        <Link to="/addkendaraan">
                            <button className="btn btn-success">Pintu Masuk</button>
                        </Link>
                    </div>
                    <table className="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Plat Nomor</th>
                                <th scope="col">Type Kendaraan</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.kendaraan.map((index, i) =>
                                <tr key={i}>
                                    {/* <td>{index.idReceptionist}</td> */}
                                    <td>{index.platNo}</td>
                                    <td>{index.typeKendaraan}</td>
                                </tr>
                            )
                            }
                        </tbody>
                    </table>
                </div>
            </>
        )
    }
}
