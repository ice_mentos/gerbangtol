import React, { Component } from 'react'
import Axios from 'axios';

export default class GerbangDua extends Component {
    state = {
        search: null,
        datasearch: [],
        // hargam: 10000,
        // hargab: 30000,
        // hargat: 40000
    }

    handleSearch = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log(e.target.value);
    }

    search = () => {
        Axios.get("http://localhost:8181/kendaraan/" + this.state.search)
            .then(res => {
                console.log(res.data);
                const datasearch = res.data
                this.setState({
                    datasearch: datasearch
                })
            })
            .then()
    }

    render() {
        return (
            <>
                <br />
                <br />
                <div className="card">
                    <div className="card-header">
                        <center><h2>Gerbang Dua</h2></center>
                    </div>
                    <div className="container">

                        <div className="card-body">
                            <form>
                                <div className="form-group">
                                    <label>Plat Nomor</label><br />
                                    <input type="text" name="seacrh" placeholder="Plat Nomor Polisi" className="form-control" onChange={this.handleSearch} />
                                </div>
                                <div className="form-group">
                                    <label>Type Kendaraan</label>
                                    {/* <input type="text" name="perusahaan" placeholder="" className="form-control" onChange={this.handleChange} /><br /><br /> */}
                                    <select className="form-control">
                                        <option>Pilih..</option>
                                        <option>Mobil</option>
                                        <option>Bus</option>
                                        <option>Truk</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label>Harga</label><br />
                                    <input type="text" placeholder="Total Bayar" className="form-control" />
                                </div>
                                <button className="btn btn-primary" type="submit" onClick={this.search}>Submit</button><br /><br />
                            </form>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
