import React, { Component } from 'react'
import Axios from 'axios'

export default class AddKendaraan extends Component {
    state = {
        platNo: "",
        typeKendaraan: ""
    }

    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        })
        console.log(e.target.value);
    }

    handleSubmit = e => {
        e.preventDefault();
        Axios.post('http://localhost:8181/addKendaraan', this.state)
            .then(res => {
                console.log(res);
                console.log(res.data);
                window.location.href = "http://localhost:3000/"
            })
    }

    render() {
        return (
            <>
                <br />
                <br />
                <div className="card">
                    <div className="card-header">
                        <center><h2>Pintu Masuk</h2></center>
                    </div>
                    <div className="container">

                        <div className="card-body">
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <label>Plat Nomor</label><br />
                                    <input type="text" name="platNo" placeholder="Plat Nomor Polisi" className="form-control" onChange={this.handleChange} />
                                </div>
                                <div className="form-group">
                                    <label>Type Kendaraan</label>
                                    {/* <input type="text" name="perusahaan" placeholder="" className="form-control" onChange={this.handleChange} /><br /><br /> */}
                                    <select className="form-control" name="typeKendaraan" onChange={this.handleChange}>
                                        <option>Pilih..</option>
                                        <option>Mobil</option>
                                        <option>Bus</option>
                                        <option>Truk</option>
                                    </select>
                                </div>
                                <button className="btn btn-primary" type="submit">Submit</button><br /><br />
                            </form>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
